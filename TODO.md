# Tk-i leckek
- [X] 47
- [X] 48
- [X] 49
- [X] 50
- [X] 51
- [ ] 52
- [ ] 53
- [ ] 54
- [ ] 55

# Egyeb
- [ ] ppt-k
- [ ] kiegészítő lap az antiszemitizmusról

# Esszék
- [x] Horthy Miklós hatalomra kerülése és a kormányzói jogkör ismertetése 
- [x] A gazdasági élet konszolidációja Bethlen István min.elnöksége idején 
- [x] Az antiszemitizmus alakulása a két világháború közötti Magyarországon
