1919 Augusztus 1-jén megbukott a tanácsköztársaság és a románok átlépték a Tiszát. Az országban anarchikus állapotok uralkodtak, a románok megkezdték a megszállt területek kifosztását.
Az ország élén nem volt stabil politikai erő, csak egymást váltó gyenge kormányok.
Azonban Szegeden már körvonalazódott egy hatalmi központ, amelynek egyre növekvő létszámú Nemzeti Hadseregét Horthy Miklós irányította.
Horthy hamarosan önállósította magát, és csapataival megszilárdította a rendet a Dunántúlon. Ennek része volt a „fehérterror” kibontakozása, ami azt jelentette, hogy tiszti különítmények és szabadcsapatok kegyetlenkedtek a tanácsállamban részt vállaló vagy ezzel meggyanúsított emberekkel.
1919.nov 16-án Horthy bevonult Budapestre, és George Clerk antant megbízott tárgyalásai után 1920 januárjára megalakult az új magyar kormány. Az új rendszer visszaállította a királyságot mint államformát, de király híján az államfői tisztséget ideiglenesen a kormányzó töltötte be. Kormányzóvá 1920. március 1-jén Horthy Miklóst választották.
Az ország államfője 1944 ig Horthy Miklós, jogköre folytonosan bővül. A kormányzónak jogában állt nemzetközi szerződést kötni (a parlament beleegyezésével), hadat üzenni, avagy békét kötni, illetve a hadsereg főparancsnoka volt.
1937 től személye védett, vagyis nem vonható felelősségre alkotmánysértésért vagy törvényszegés esetén.
A kormányzót bizonytalan időre választották, tehát leválltani nem lehetett, ám utódajánlási joga megvolt, kormányzóhelyettesnek fiát választja 1942-ben.
Horthy rendelkezett a kegyelmezés jogával, de nem adományozhatott nemesi címet, helyette megalapította a Vitér Rendet, a vitézavatásokon a fegyveres szolgálataikért kitüntetett tagok földet és vitéz előnevet kaptak.
A kormányzó a törvényhozó hatalmat is nagyban korlátozta. A törvényeket akár 2szer is visszaküldhette 6 hónapon belül, a törvények kihirdetését elhalaszthatta 6 hónapig, elnapolhatta az országgyűléseket, illetve új felsőházi tagokat nevezhetett ki. Vétó joga azinban nem volt. A törvények zömét általában előre jóváhagyatták vele.
Ő irányította a politikai életet, Horthy támogatása volt az alapfeltétel a pártoknak a hatalomra jutáshoz. A miniszterelnököket is ő választotta, vagy váltatta le. (Bethlen,Károlyi,Gömbös,Darányi,Imrédy)
Kormányzása alatt próbál semleges maradni, leváltatja Imrédyt az ellenzék segítségével miután ő elfogadja az első Zsidó törvényt és túlzottan közel kerül a németekhez.
Próbál nyitni a Nyugat felé, de az 1941es bécsi döntés után elköteleződik a tengelyhatalmak mellett, a revízió reményében.
