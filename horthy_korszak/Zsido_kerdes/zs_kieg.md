I Zsidótörvény - 1938

II Zsidótörvény - 1939 máj

Volskbund : német/náci szerű magyar munkáspárt

Nyilas keresztesek: alapítója és vezetője Szálasi Ferenc, szélső jobb oldal, többször bebörtönzik, vagy betiltják városokon belül a gyűléseit a németekhez való túlzott kötődés miatt.
Az első párt ami faji alapon különbözteti meg a zsidóságot. Német minta: NSDAP programjainak és propagandájának átvétele, saját rohamosztag, jelképek, köszöntések stb.
pártállami törekvések, totalitárius követelések, vagyoni egyenlősítés ( zsidók rovására)
Teleki kormánya alatt megerősödnek, több százezer tag ,,zöld bolsevizmus"

Numerus Clausus - 1920

Imrédy-t zsidó felmenői miatt váltják le

A magyar külpolitikai és revizióvágy egyre jobban Németországhoz csatolta magyarországot. FElerősödnek a szélsőségek és az antiszemitizmus.
1942-1943-tól már érezhető volt a német nyomás: Berlin követelte, hogy Magyarország német mintára „oldja meg a zsidókérdést”. Horthy és konzervatív köre azonban elutasította, hogy a magyar zsidókat átadja a németeknek. Pedig a szélsőjobboldali pártok, a kormánypárt nácibarát csoportjai, a sajtó, a közvélemény és a közigazgatás jelentős része szívesen látott volna az érvényben lévő szabályoknál keményebb zsidóellenes fellépést is. Az ő idejük 1944 márciusában jött el, amikor a német megszállás után megkezdődött a magyar zsidóság megsemmisítése. 

