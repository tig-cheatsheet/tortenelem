# 1934 - Miniszterelnök: Gömbös Gyula
- szovjetúnióval való kapcsolatfelvétel
- Római jegyzőkönyv (olaszo, ausztria, mo): politikai kapcsolatok erősítése
- Német-Magyar kereskedelmi pótegyezmény (Gömbös Hitler látogatás, etnikai alapú revízió)

# 1938
## Március 15 - Anscluss
- ausztria, németo egyesül

## Szeptember
- Müncheni konferencia (anglia, franciao, olaszo): nyugat cseh területekről tárgyalás, szudéta térség németországhoz csatolása, magyar revíziós törekvések támogatása

## November 2 - I. Bécsi döntés
- németo => Ribbentrop
- olaszo => Ciano

Felvidék déli részét visszacsatolják (11927km^2, 80% magyar -> etnikai alapon történik)
- Pozsony, Kassa, Ungvár

## Új miniszterelnök: Imrédy Béla
- Forradalom
- Szélsőséges irányba elmegy a vezetése (**rendeleti módon** próbál kormányozni (mint Hitler))
- Horthyéknak ez nem tetszik
- Egyik felmenője zsidó, lemondatják

### I. zsidótörvény (lemondása előtt)
- 20%-os korlát
- vallási alapon

### Volksbund (lemondása előtt)
- Magyarországon élő német közösségek, munkáspárt szervezése

# 1939 - Teleki Pál 2. miniszterelnöksége

## Február
- mo. csatlakozik az **antikomintern paktum**hoz (kommunista ellenes)

## Március
- Csehszlovákia további feldarabolása
- TISO (=nemzeti szlovák párt -> független szlovák köztársaság)

### Március 15
- **Cseh-Morva protektorátus** => Csehszlovákia *megszűnik*
- mo. bevonul Kárpátaljára (12000 km^2 elfoglalva -> kevés magyar --> **nem etnikai revízió**)  
    --> mo. határos lengyelo.-val

## Április
- mo. kilép a népszövetségből (kész a háborúra németo. oldalán)

## Május
- II. zsidótörvény (6%-os korlát, faji alapon), *hasonlóan kell működni, mint németo.*

## Szeptember 1. - A II. vh. kitörése
- németo. hadüzenet nélkül megtámadja lengyelo.-t

Teleki Pál a ***fegyveres semlegesség gondolata*** jegyében:
- nem enged át német fegyveres haderőket lengyelo. irányába
- de a lengyel-magyar határt megnyitja a lengyel menekülők számára

# 1940 - Teleki Pál

## Augusztus 30 - II. Bécsi Döntés
- mo. még mindig semleges
- cél: Erdély visszavétele
- `1940.08.30`: II. Bécsi Döntés (Ribbentrop, Ciano): Északerdély, Székelyföld visszacsatolása (43000 km^2)

## November - 3 hatalmi szövetség
Emiatt mo.-nak csatlakozni **kell** a ***3 hatalmi szövetség***hez (német, olasz, japán)
- (ez egy katonai hadi szövetség => mo.-nak harcolni kell a háborúban)

## December - Jugoszláv-Magyar örök barátsági egyezmény
