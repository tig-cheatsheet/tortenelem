# Trianon utáni helyzet
- magyarság harmada kissebségi sorsba kényszerült
- határtól távol jelentős: **székelység**

- megváltozott a hivatalos nyelv, iskolai oktatás, közigazgatás
- akit korábban az állam foglalkoztatott, az elvesztette a munkáját
- az **utódállamok a versailles-i békerendszer kisebbségvédelmi intézkedéseit kijátszották**
    - felszámolták a magyar iskolahálózatot
    - magyarok érvényesülését akadályozták a helyi közigazgatásban

- **telepítési politika** -> ezzel próbálták feloszlatni a határ menti egységes magyar tömböket
- háború utáni földosztások: etnikai szempontokkal, magyarok kárára

- az utódállamokban a **magántulajdonon alapult a tásadalom**, így a kissebségi magyarság **saját erejéből** fenntarthatott iskolákat, újságokat, színházakat
    - **egyházakat nem szorították vissza**: egyházi közösségek -> *magyarságot megtartó legfontosabb erő*

# A kulturális élet
- mo.-ra költözők (**repatriálók**) nagyrésze értelmiségi -> nehezíti a kissebségbe került magyarság kulturális, politikai szerveződését
- az **oktatási rsz átalakítása** miatt az **értelmiségi utánpótlás csökken**
- 20-as évek: Erdélyben élénk irodalmi élet -> (Tamási Áron, Áprily Lajos, Dsida Jenő, Kós Károly)
    - egyházi vagyon, alapítványok: megmaradt magyar iskolák támogatása
    - magyar nemzetiségi mozgalomnak hátteret biztosított
    - PL: **Erdélyi Helikon** (folyóirat, 1928-1944); **Erdélyi Szépmives Céh** (1924)
