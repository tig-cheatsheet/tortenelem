# A trianoni béke gazdasági hatásai
- **Párizs környéki békék**: Osztrák-Magyar Monarchia feldarabolása
- a korábbi nagy piacra termelő iparágak túlméretezetté váltak, a kisebbek fejlesztésre szorultak
- DE: a tőkehiány miatt nagyon kevés volt a fejlesztés
- a vállalatok csak a szűk belső piacra termeltek, nem tudtak kitörni a világpiacra  
    ==> nagyon lassú fejlődés = **stagnálás** vagy leszakadás

# Mezőgazdaság
- (szántók, szőlők maradtak, de az erdőkből kevés maradt)
- mo. számára hatalmas veszteségek
- az OMM. egységes, szabad, belső biztosító hatalmas piaca megszűnt
- **a magyar piacon nem lehetett eladni a gabonafelesleget**
- tőkehiány miatt meg nem lehetett fejleszteni, ezért a **világpiacon nem adhattuk olcsóbban**
- a magyar gabona nem volt verseny az olcsó amerikai áraknak  
    ==> magyar mezőgazdaság: II. vh.-ig értékesítési gondok

# Bányászat
- mo. bányakincseinek nagyrésze határon kívülre került (nemesfém, só, vasérc, szén)
    - (tk. 253 térkép)

# Ipar
Ágazatonként eltérő mértékben érintették a határváltoztatások

Jelentős csökkenés:
- faipar: csökken (Kárpátok)
- felvidéki könnyűipar: csökken (Felvidék)

Kis csökkenés:
- nehézipar
- gépgyártás
- kohászat

Nincs csökkenés:
- élelmiszeripar

# Összefoglalva
- néhol túlméretezett iparágak, máshol hiány
- de a **térség államai nem működtek együtt**
- ezért nem voltak hatékonyak, nem tudtak kitörni a világpiacra

- I. vh. után mo. mély gazdasági válságba került

# Településszerkezet torzulásai
- A trianoni béke: városhálózat fejlődésének megakasztása, torzítása
- Szinte az egész ország Budapest vonzáskörzetébe került (rossz irány)
- Nagyvárosok elvesztették a vonzáskörzetüket, és fejlődésképtelen településekké váltak
- új határok miatt elvesztették a piackörzetük jelentős részét (Kassa, Nagyvárad, Komárom)
- **Megszűnt a szabad munkaerő vándorlás** (nehéz volt a határokon átjutni -> pl:felvidéki aratók elzárása)

# Közlekedés veszteségei
- **értékes vasútvonalak elcsatolása** volt az *utódállamok egyik célja*
- területi igényeik alátámasztására érvként használták a béketárgyalásokon
