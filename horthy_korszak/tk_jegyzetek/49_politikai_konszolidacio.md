# Teleki Pál miniszterelnöksége - a konszolidáció kezdete
- 1920-ban a politikai helyzet még nem szilárdult meg
- volt már kormány, államfő(Horthy Miklós) is, de sok minden hiányzott

A politikai konszolidáció elindítása: Teleki Pál gróf (1920. július - 1921. április)
- Teleki Pál a háború előtti vezető réteg tagja volt
- Miniszterelnőkként: szélső jobb és baloldal visszaszorítása
- A különítményeket karhatalommal felszámoltatta
- Kommunista pártot betiltatta, politikai rendőrséggel üldöztette

# Rongyos Gárda
- 1919 tavaszán alakult a Székely Hadosztály maradványaiból
- cél: ország területének védelme
- vezetők: Prónay Pál, Héjjas Iván
- Ausztriának átadásra kijelölt területet megszállták
- Önkéntesek érkeztek hozzájuk az ország minden részéről
- Fegyverrel megakadályozták az osztrák katonaság és csendőrség bevonulását (`1921. aug-szept`)

A Rongyos Gárda fellépése kellemetlenné vált az antant számára, így olasz támogatással Velencében leültek tárgyalni a magyar kormány képviselőivel (`1921. okt. 11-12`)
- mo. vállalta, hogy kivonja a Rongyos Gárdát a területről  
    - az antant hozzájárult a Sopronról és környékéről szóló népszavazáshoz

# Népszavazás Sopronról
- franciák megrémültek attól, hogy az osztrákok a németországgal való egyesülésben látták a jövőt (anschluss), ezért ezt meg is tiltották, és kiengesztelésképp adtak nekik is földet magyarországból
- a francia politika mo.-n is szeretett volna befolyásra szert tenni, ezért nem zárkózott el kisebb változtatásoktól  
    - (`1921. dec. 14-16`): engedélyezték Sopronban és környékén a népszavazást
    - a városban és környékén a német anyanyelvűek voltak többségben, ennek ellenére a népszavazáson a többség mo. mellett döntött.
    - Sopron: *Leghűségesebb Város* (Civitas Fidelissima) cím

# Nagyatádi-féle földreform
- Nagyatádi Szabó István
- 1920-ban mérsékeltebb földreform, mint amilyet korábban javasolt.
- Csak a nagybirtokok kis hányadát osztották fel  
    - sok földnélkülit nem sikerült földhöz juttatni

- mo.-n a nagybirtokrendszer, ezzel a nagybirtokosok gazdasági és politikai befolyása érintetlen maradt

# Numerus Clausus (=zárt szám)
- egyetemi felvételiket szabályozó törvény)
- származási és politikai megkülönböztetés
- a felvehető hallgatók számát egyes **népfajok** és nemzetiségek országos arányához kötötték
- zsidó értelmiséget keresztényre szerették volna cserélni

# IV. Károly visszatérési kísérlete - kormányváltás
- királypuccsok
- a Habsburg-uralkodó visszatérését a magyar trónra az antant, és főként a szomszédos utódállamok elfogadhatatlannak tartották, mert abban a Monarchia feltámasztását látták
- a kisantant állami mozgósítással és katonai beavatkozással fenyegetőzött
- az 1. alkalom (`1921. március`): Horthy lebeszéli Károlyt  
    - Kiderült, hogy a magyar vezető rétegben számottevő a királypártiak (**legitimisták**) száma
    - Teleki Pál is megingott  -> Horthy Miklós bizalmát elvesztette --> Bethlen István gróf került Teleki helyére
- a 2. alkalom (`1921. október`): kisebb fegyveres összecsapás, halottak is voltak  
    - (`1921.nov.6.`): A 2. kísérletet követően a **magyar törvényhozás kimondta a Habsburg-ház trónfosztását**

---

# Összegezve
- Trianoni békediktátum
- Teleki Pál vezetésével indult meg a konszolidáció
- Teleki visszaszorította a szélsőjobboldalt, a különítményeket és betiltotta a kommunista pártot
- Földreform a parasztság megnyugtatására, de ez **nem** oldotta meg a parasztság alapvető problémáit, nagybirtokrendszer érintetlen maradt
- Erősödő [antiszemitizmus](antiszemitizmus_lap.md)  
    - Numerus Clausus
- IV. Károly külpolitikai okok miatt sem térhetett vissza
