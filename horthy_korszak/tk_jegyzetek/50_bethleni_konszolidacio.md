# Politikai stabilizáció kibontakozása
- parlamentarizmus megerősítése
- szélsőségek távoltartása a parlamenttől (Teleki Pál politikáját követte)
- erős kormánypártra volt szüksége -> ezt csak úgy tudta, hogy belépett Nagyatádi Szabó István **Kisgazdapárt**jába  
    - **Egységes Párt** létrejötte

- a kisgazdák együttműködtek a válságig (1930) Független Kisgazdapárt
- `1921`: Bethlen-Peyer-egyezség  
    - kiegyezett a parlamentarizmust elfogadó baloldali párttal, a **szociáldemokratákkal**
    - a szociáldemokraták vállalták, hogy nem szervezkednek a következő körökben:  
        - földművesek
        - közalkalmazottak
        - vasutasok
    - Bethlen biztosította a párt szabad működését és a parlamenti képviselet lehetőségét

- `1922`: rendelet a szavazati jog korlátozására:  
    - a kormány **vagyoni cenzus**hoz kötötte a választójogot, és a **nőknél 30 év**re emelte a választójog **korhatár**át

==> kormánypárt erősítése, ellenzék gyengítése

A **korlátozott parlamentarizmus** biztosította a szélsőségek visszaszorítását, de a társadalom jelentős rétegeit kizárta abból, hogy érdekeiket érvényesítsék a parlamenti politikában.

# Az oktatás fejlesztése
- modernizáció
- legtöbb ember számára: felemelkedés lehetősége = oktatás
- felsőfokú diploma = középréteg (származástól függetlenül)
- jelentősen nőtt a magasabb iskolai végzettségűek száma (így is elmaradt a nyugat-európai szinttől)
- `1920`-as években növelték az oktatásra szánt összegeket

## Klebelsberg Kuno
- a magyarság felemelése, régi szerepének visszaszerzése csak a népesség kulturális szintjének emelésével lehet
    - **kultúrfölény fenntartása**

- az **analfabetizmus harmadára csökkent**
- a korszak végén megkezdték a **kötelező nyolcosztályos általános iskola** bevezetését

Egyetemek átköltöztek:
- Pozsonyi egyetem --> Pécs
- Kolozsvári egyetem --> Szeged

# Összefoglalva
- Bethlen folytatta Teleki által megkezdett konszolidációt
- Kisgazdapártba belépve erős kormánypártot hozott létre
- Hogy a kormány többségét fönntartsa, korlátozta a választójogot, visszaállította a nyílt szavazást
    - Korlátozott parlamentarizmus
- A korszak sikerágazata az oktatás
