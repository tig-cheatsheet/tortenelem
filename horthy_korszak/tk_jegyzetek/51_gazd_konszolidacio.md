# Szerkezetváltás
- túlmértetezett gazdaságok (élelmiszeripar, nehézipar, gépgyártás)
- cél: könnyűipar korszerűsítése: tőkeszegénység miatt csak a nyugat eu-ban leselejtezett gépeket alkalmazták

## élelmiszeripar
- malomipar **nem** fejlődött
- de a konzervipar **igen** (Nagykőrös, Szeged)
- kormányzat támogatta a **belterjes mezőgazdaság** fejlesztését

# Inflációs politika
- gazdasági szerkezetváltás komoly tőkét igényelt
- ez nem volt meg a 20-as évek elején
- emiatt **inflációs politikát** alkalmaztak
    - kormányzat növelte a pénzkibocsátást -> gyorsuló infláció
    - így tudtak hiteleket adni a vállalkozóknak, amit (előnyösen) az inflálódó pénzben fizettek vissza

# Népszövetségi kölcsön felvétele
- a gazdasági konszolidáció elengedhetelen feltétele: **külföldi tőke bevonása**
- feltétel: bekerülni a **Népszövetségbe**
    - Bethlen igyekezett eleget tenni a nyugati elvárásoknak
    - siker: (`1922`): mo.-t felvették a Népszövetségbe

- sikernek számított a népszövetségi kölcsön felvétele (250 millió aranykorona) (angol támogatás, kisantant gátolni próbált)

# Stabilizáció
- `1925`-re megállították a korona inflációját, rögzítették az árfolyamát
- `1927`: új értékálló valuta bevezetése: **pengő** (2.vh-ig megtartotta az értékét)

Hitelt nyújtottak:
- Mezőgazdaság moderniazálása
- turizmus
- villamosipar
- közlekedés

A kölcsön **segítette a szerkezetváltást**. (textiliparra való átállás)

+Repatriáltak (menekült magyarok) elhelyezése

- gazdaság helyzete megszilárdult (**stabilizálódott**)
- `1929`-re számos területen az ország túlszárnyalta az utolsó békeév eredményeit
    - textilipar
    - elektronika

# Bethlen külpolitikája
- cél: elszigeteltség oldása, **egyenjogúság kivívása**
- anglia részben támogatott: népszövetségi kölcsön
- fro. a kisantant mögött állt

- olaszo. ideális partner: győzelmével elégedetlen
    - délszlávokkal szemben lehet rá számítani
    - de: románok, csehszlovákokkal szemben nem

- `1927`: magyar elszigeteltséget megtörő **olasz-magyar örök barátsági szerződés** (Bethlen - Mussolini)
