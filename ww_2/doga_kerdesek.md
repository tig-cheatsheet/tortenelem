1. Milyen szándék vezérelte Hitlert és Sztálint a megnemtámadási egyezmény megkötésekor?
     - Molotov-Ribbentrop paktum ( Moszkva `1939.08.23`)(nyilvános és titkos rész) 
     - Hitler: Biztosítani akarta, hogy Oroszország nem támadja hátba Németországot amig azok nyugaton háborúznak, Németország és Oroszország 2 oldalról könnyedén bevették Lengyelországot.
     - Sztálin: Lengyel területek megkaparintása. A háborúra való fel nem készültség miatt félt a Németországgal való háborútól.
     - Mind2: Lengyel területek, 2 frontos háború elkerülése. A másik fél angol/francia szövetségek megakadályozása 
2. Hogyan rohanta le Németország a lengyel államot?
    - (`1939.09.01` 2. világháború kitörése)
    - Gleiwitz-i rádiótorny megtámadása csel (Casus belli)(egynappal előtte `1939.08.31`)
    - A németek ezek után lerohanták Lengyelországot, a túloldalról nem sokkal később az Oroszok követték példájukat. A lengyelek a rendkívüli túlerő és régimódi fegyvernemei (lovas katonák) miatt esélytelenek a támadások visszaverésére.
3. Mit jelent a furcsa háború fogalma?
    - (`1939.09.03 - 1940.05.10`) Az angolok és franciák(lengyel szövetségesek) hadat üzennek Németországnak, de valódi hadi cselekvések nem történnek.
4. Milyen lépésekben tolta ki a Szovjetunió a Nyugati határait?
    - Finn.o Lett.o Észt.o Lengyelország keleti részének bekebelezése, de ebből Finn.o nem jön neki össze.
5. Hogyan készítette elő és hajtotta végre Hitler a nyugati hadjáratot?
    - előkészítés: Orosz megnemtámadási egyezmény, szövetségek kötése(Olasz.o Japán, Magyar.o) Dánia és Norvégia elfoglalása nyersanyagok miatt.
    - végrehajtás: Dánia->Norvégia és Svédország -> Belium (a Francia Maginot vonal megkerülése) -> Francia.o északi és keleti részeinek elfoglalása(Francia bábállam, Henry Philippe Pétain vezetésével(vichy kormány)), Anglia Bombázása(ez az angol fejlett légierő és a radar miatt abba maradt)
6. Mely irányban indult a német támadás a Szovjetunió ellen, és milyen eredményekkel zárultak a hadműveletek?
    - Barbarossa hadművelet(`1941.06.22 - 1941.12.05`): 3 irányban támad németorzság a Szovjetunióra. 
    - Északfelé: Leningrad
    - Keletre:  Moszkva
    - Déli: Kijev -> Sztálingrád
    - Rövid idő alatt beérnek Moszkváig ( látják a kupolákat)(ugyanis a szojetek nagyon alulkészültek a háborúra, ugyanakkor sokkal több erőforrás áll rendelkezésükre), de megtorpannak mert nem számoltak az orosz téllel és a felperzslet föld taktikával.
    - Moszkvát és Leningrádot(Lenin meghagyja nekik hogy a végsőkig védeni kell) nem sikerül bevenniük, Kijevet és Sztálingrádot viszont igen (hatalmas pusztítás)
7. Milyen okok miatt lépett be az USA a háborúba?
    - Pearl harbor (`1940.12.07`) Japán hadüzenet nélkül lebombázza az ott álomásozó amerikai flottát.
    - A németek több US hajót elsüllyesztettek tengeralattjáróikkal
    - Félelem a német terjeszkedéstől
8. Miért tekintik a második világháborút a történelem addigi háborúitól eltérő jellegűneK?
    - A fejlődő hadi ipar és fegyverek miatt megszűntek az állóháborúk.
    - A hátország sokkal nagyobb áldozatot fizetett pl. bombázások vagy a hadsereg ellátása miatt.
9. Mikor fogalmazták meg és hogyan hajtották végre a németek az Endlösung programját?
    - Végső megoldás vagyis "Endlösung" (`1942.01`) bár a zsidóság sorsa már `1941.09`kor eldőlt, ezen a wanseei konferencián beszélték meg a tervet és az eszközöket.
    - Mivel a zsidók lelövése túl sok időt vett igénybe és nagyon megviselte a német katonákat, módszert váltottak a nácik. Az összegyűjtött zsidókat gázkamrában ölték meg, majd holttestüket feldolgozták, vagy elégették. Akiket nem öltek meg azonnal (a munkára alkalmasnak vélt zsidókat), azokat munkatáborokba vitték ahol halálukba dolgoztatták őket. pl(Auschwitz, Treblinka, Belzec)
10. Milyen okok játszottak szerepet az atombomba ledobásában? 
    - Japán makacs volt és a szövetségesek nem tudtak döntő győzelmet kicsikarni, illetve a kamikaze pilóták túl nagy károkat okoztak. Japán megszálása túl sok erőforrást emésztett volna fel, ez egy sokkal gyorsabb, egyszerűbb, olcsóbb, de annál brutálisabb megoldás volt, a háború gyors lezavarására.
    - Amerika kíváncsi volt az atom valódi erejére.
    - Visszavágás Pearl Harbor - ért.


