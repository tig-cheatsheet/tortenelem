1. | `1939.augusztus.23` (a szovjet–német megnemtámadási egyezmény)
2. | `1939. szeptember 1.` (Németország megtámadja Lengyelországot, kitör a második világháború)
3. | `1941. június 22.` (Németország megtámadja a Szovjetuniót)
4. | `1942. január` (a wansee-i értekezlet)
5. | `1942` (a Midway-szigeteknél lezajlott ütközet, az el-alameini csata)
6. | `1943` (véget ér a sztálingrádi csata, a kurszki csata) 
7. | `1943` (a szicíliai partraszállás, Olaszország kilépése a háborúból, a németek megszállják Olaszországot)
8. | `1943 vége` (a teheráni csúcstalálkozó)
9. | `1944. június 6.` (megkezdődik a szövetségesek normandiai partraszállása)
10. | `1945. február` (a jaltai konferencia)
11. | `1945. május 9.` (az európai háború befejeződése)
12. | `1945. augusztus 6.` (atomtámadás Hirosima ellen)
13. | `1945. szeptember 2.` (Japán fegyverletételével véget ér a második világháború)
14. | `1945` (az ENSZ megalakulása)
15. | `1945. július` (a potsdami konferencia)


 
