1. Leningrád:
    - Barbarossa egyik célja, hogy elfoglaljak
    - BLOKÁD: `1941.09.08 - 1944.01.27`
    - véres német ostrom, oroszo északi része
2. Moszkva:
    - Taifun hadművelet: `1941.10.02`
    - *Moszkvai csata* (végső roham Moszkva ellen)
3. Lengyelország:
    - Szovjetúnió és Németország között
    - 1. német célpont
    - szegény hadsereg -> 0 védelem (lovak vs tankok)
4. Balkán:
    - Olasz invázió
    - Brit partraszállás
    - Görögo megtámadása
5. Danzig:
    - *Danzig Szabad Város*
    - `1920.11.15`-től a II. vh kezdetéig különleges státusz (németo, és lengyelo-tól különálló terület)
    - Háború végén a Vörös Hadsereg megszállása alatt
6. Dánia:
    - **Weserübung hadművelet**
    - enyhe német megszállás (`1940-43`)
    - zsidók átmenekítése Svédországba
    - `1945.05.04` - felszabadulás
7. Norvégia:
    - **Weserübung hadművelet**
    - `1940.04.09` - Németo: Dánia és Norvégia lerohanása
        - hogy megelőzze a fr és brit megszállást
8. Belgium:
    - `1940.05.10` - német támadás
    - Waffen SS
9. Franciaország:
    - `1940.05.10` - franciaországi hadjárat
    - fro lerohanása mellett hollandia belgium és luxemburgot is elfoglalták a németek
    - *Fall Gelb* --> német hegységen (Ardennek) átkelés, hátulról bekerítés
    - *Fall Rot* --> Maginot vonal **kikerülése** xd --> hátbatámadás megint
10. Pearl Harbor:
    - **Ai hadművelet**
    - `1941.12.07`: Japán támadás az USA Csendes-óceáni Flottájának Hawaii-szigeteken lévő fő támaszpontja ellen 
    - Meglepetésszerű --> flotta nagyrészének tönkretétele
11. Midway:
12. El-Almein:
13. Sztálingrád:
14. Don-kanyar:
15. Kurszk:
16. Normandia:
17. Jalta:
18. Potsdam:
19. Berlin:
20. Hirosima:
21. Teherán:
22. Nagasaki:
23. Auschwitz:
24. Drezda:
25. Gleiwitz:
    - `1939.08.31` - németek lengyel ruhában letámadták az adótornyot --> casus belli Lengyelo lerohanására
